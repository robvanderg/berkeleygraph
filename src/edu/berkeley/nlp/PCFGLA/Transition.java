package edu.berkeley.nlp.PCFGLA;

public class Transition {

	public short beg = -1;
	public short end = -1;
	public String word = "";
	public double prob = 0.0;

	public Transition(String line) 
	{
		String[] splitted = line.split(" ");
		beg = Short.parseShort(splitted[0]);
		word = splitted[1];
		word = word.replace(")", "-RRB-");
		word = word.replace("(", "-LRB-");

		end = Short.parseShort(splitted[2]);
		if (splitted.length > 3)
			prob = Double.parseDouble(splitted[3]);
		else
			prob = 1.0;
	}
}
