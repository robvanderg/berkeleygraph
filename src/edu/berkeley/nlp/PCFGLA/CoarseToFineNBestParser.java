package edu.berkeley.nlp.PCFGLA;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.zip.GZIPOutputStream;

import edu.berkeley.nlp.syntax.StateSet;
import edu.berkeley.nlp.syntax.Tree;
import edu.berkeley.nlp.util.ArrayUtil;
import edu.berkeley.nlp.util.Numberer;
import edu.berkeley.nlp.util.PriorityQueue;
import edu.berkeley.nlp.util.ScalingTools;

/**
 * @author petrov
 * 
 */
public class CoarseToFineNBestParser implements Callable<Object>{
    /* The number of k best parses to return */
	int kBest;
    /* */
    List<Double> maxRuleScores;
    /* Numberer to convert int stateIds to string labelNames */ 
    Numberer tagNumberer = Numberer.getGlobalNumberer("tags");
    /* Length of the lattice in number of states */
    short length;
    /* Maximum length of a lattice in states, larger lattices are not parsed*/
    int myMaxLength = 1000;
    /* Word level labeller */
    Lexicon lexicon;
    /* Number of constituents */
    int numStates;
    /* Maximum number of subStates possible for 1 constituent (state) */
    int maxNSubStates;
    /* Actual number of subStates per contituent (state) */
    short[] numSubStatesArray;
    /* */
    int[] idxC;
    /* */
    double[] scoresToAdd;
    /* */
    Grammar grammar;
    /* Number of levels to parse/prune */
    int nLevels;
    /* Saves which states are phrase level and preterminal */
    final boolean[] grammarTags;
    /* Should the tree bee a viterbiParse? */
    final boolean viterbiParse;
    /* Output sub states? */
    final boolean outputSub;
    /* Output the score of the tree? */
    final boolean outputScore;
    /* Decides which threshold to prune on */
    final boolean accurate;
    /* current level */
    int level;
    /* Because the same words should often be saved in words/vWords */
    WordId wordIds;
    /* Decide to parse viterbi style, or add the probabilities */
    public boolean viterbi = false;
    /* The next sentence to parse */
    Lattice nextLattice;
    /* Id, to keep the order of the sentences right */
    int nextSentenceID;
    /* Id of the parser*/
    int myID;
    /* */
    PriorityQueue<List<Tree<String>>> queue;
    /* */
    int startLevel;
    /* */
    int endLevel;
    /* Thresholds for pruning*/
    double[] maxThresholds;
    /* Probability of parse/sentence */
    double logLikelihood;
    /* Best tree found */
    Tree<String> bestTree;
    /* */
    boolean isBaseline;
    
    /* Saves the allowed substates (pruning) */
    boolean[][][][] allowedSubStates;
    /* Saves the allowed states (pruning) */
    boolean[][][] allowedStates;
    /* Saves the allowed states for the 0th level (pruning) */
    boolean[][] vAllowedStates;
    /* Grammars for each levels */
    Grammar[] grammarCascade;
    /* Lexicons for each level*/
    Lexicon[] lexiconCascade;
    /* */
    int[][][] lChildMap;
    /* */
    int[][][] rChildMap;
    /* Saves the words used: start, end, state, substate */
    int[][][][] words;
    /* Saves the words used: start, end , state */
    int[][][] vWords;
    /* inside scores level 0: start, end, state -> logProb*/
    double[][][] viScore; 
    /* outside scores level 0: start, end, state -> logProb*/
    double[][][] voScore; 
    /* inside scores; start idx, end idx, state, substate -> logProb */
    double[][][][] iScore;
    /* outside scores; start idx, end idx, state, substate -> logProb */
    double[][][][] oScore;
    /* the rightmost left extent of state s ending at position i */
    int[][] narrowLExtent = null; 
    /* the leftmost left extent of state s ending at position i */
    int[][] wideLExtent = null; 
    /* the leftmost right extent of state s ending at position i */
    int[][] narrowRExtent = null;
    /* the rightmost right extent of state s ending at position i */
    int[][] wideRExtent = null;
    /* For maxC calculations */
    LazyList[][][] chartBeforeU;
    /* For maxC calculations */
    LazyList[][][] chartAfterU;
    /* */
	double latticeWeight;
	
	boolean outputChartSize;

	public Binarization binarization;
	List<Posterior> posteriorsToDump;

    /* If we have gold pos tags, we force to use them */
    final boolean useGoldPOS;
	public CoarseToFineNBestParser(Grammar gr, Lexicon lex, int k,
			int endL, boolean viterbi, boolean sub,
			boolean score, boolean accurate,
			boolean useGoldPOS, boolean initCascade, double latticeWeight, boolean outputChartSize2) {
		grammar = gr;
		lexicon = lex;
		// this.numSubStatesArray = gr.numSubStates.clone();
		// System.out.println("The unary penalty for parsing is "+unaryPenalty+".");
		this.accurate = accurate;
		this.viterbiParse = viterbi;
		this.outputScore = score;
		this.outputSub = sub;
		this.useGoldPOS = useGoldPOS;
		this.outputChartSize = outputChartSize2;

		this.tagNumberer = Numberer.getGlobalNumberer("tags");
		this.numStates = gr.numStates;
		this.maxNSubStates = maxSubStates(gr);
		this.idxC = new int[maxNSubStates];
		this.scoresToAdd = new double[maxNSubStates];
		this.grammarTags = new boolean[numStates];
		for (int i = 0; i < numStates; i++) {
			grammarTags[i] = gr.isGrammarTag(i);
		}
		grammarTags[0] = true;

		nLevels = (int) Math.ceil(Math.log(ArrayUtil.max(gr.numSubStates))
				/ Math.log(2));
		this.grammarCascade = new Grammar[nLevels + 3];
		this.lexiconCascade = new Lexicon[nLevels + 3];
		this.maxThresholds = new double[nLevels + 3];
		this.lChildMap = new int[nLevels][][];
		this.rChildMap = new int[nLevels][][];
		this.startLevel = -1;
		this.endLevel = endL;
		if (endLevel == -1)
			this.endLevel = nLevels;
		this.isBaseline = (endLevel == 0);

		if (initCascade)
			initCascade(gr, lex);
		this.kBest = k;
			
		wordIds = new WordId();
		this.latticeWeight = latticeWeight;			
	}
	
	public List<Tree<String>> getKBestConstrainedParses(Lattice nextLattice2,
			List<String> posTags, int k) {
		if (nextLattice2.size() == 0) {
			ArrayList<Tree<String>> result = new ArrayList<Tree<String>>();
			result.add(new Tree<String>("ROOT"));
			return result;
		}
		doPreParses(nextLattice2, null, false, posTags);
		List<Tree<String>> bestTrees = null;
		double score = 0;

		Grammar curGrammar = grammarCascade[endLevel - startLevel + 1];
		Lexicon curLexicon = lexiconCascade[endLevel - startLevel + 1];
		// numSubStatesArray = grammar.numSubStates;
		//clearArrays();
		double initVal = (viterbiParse) ? Double.NEGATIVE_INFINITY : 0;
		int level = isBaseline ? 1 : endLevel;
		
		createArrays(false, curGrammar.numStates, curGrammar.numSubStates,
				level, initVal, false);
		initializeChart(nextLattice2, curLexicon, false, false, posTags, false, false);
		//printChart(curGrammar, false);
		doConstrainedInsideScores(curGrammar, viterbiParse, viterbiParse);
		
		score = iScore[0][length][0][0];
		if (!viterbiParse)
			score = Math.log(score);// + (100*iScale[0][length][0]);
		logLikelihood = score;

		if (score != Double.NEGATIVE_INFINITY) {
			if (!viterbiParse) {
				oScore[0][length][0][0] = 1.0;
				doConstrainedOutsideScores(curGrammar, viterbiParse, false);
				//printChart(curGrammar, false);
				doConstrainedMaxCScores(nextLattice2, curGrammar, curLexicon,
						false);
			}
		}
		else
			return new ArrayList<Tree<String>>();
		if (outputChartSize)
			System.out.print(chartSize(curGrammar, true) + "\t");
		grammar = curGrammar;
		lexicon = curLexicon;
		bestTrees = extractKBestMaxRuleParses(0, length, nextLattice2, k);
		return bestTrees;
	}


	public void doPreParses(Lattice nextLattice2, Tree<StateSet> tree,
			boolean noSmoothing, List<String> posTags) {
		clearArrays();
		length = (short) (nextLattice2.size());
		double score = 0;
		Grammar curGrammar = null;
		Lexicon curLexicon = null;
		double[] accurateThresholds = { -8, -12, -12, -11, -12, -12, -14, -14 };
		// double[] accurateThresholds = {-10,-14,-14,-14,-14,-14,-16,-16};
		double[] fastThresholds = { -8, -9.75, -10, -9.6, -9.66, -8.01, -7.4,
				-10, -10 };
		// double[] accurateThresholds = {-8,-9,-9,-9,-9,-9,-10};
		// double[] fastThresholds = {-2,-8,-9,-8,-8,-7.5,-7,-8};
		double[] pruningThreshold = null;

		if (accurate)
			pruningThreshold = accurateThresholds;
		else
			pruningThreshold = fastThresholds;

		// int startLevel = -1;
		for (level = startLevel; level <= endLevel; level++) {
			if (level == -1)
				continue; // don't do the pre-pre parse
			if (!isBaseline && level == endLevel)
				continue;//
			curGrammar = grammarCascade[level - startLevel];
			curLexicon = lexiconCascade[level - startLevel];

			// createArrays(level==startLevel,curGrammar.numStates,curGrammar.numSubStates,level,Double.NEGATIVE_INFINITY,false);
			createArrays(level == 0, curGrammar.numStates,
					curGrammar.numSubStates, level, Double.NEGATIVE_INFINITY,
					false);
			initializeChart(nextLattice2, curLexicon, level < 1, noSmoothing,
					posTags, false, true);
			final boolean viterbi = true, logScores = true;
			if (level < 1) {
				doConstrainedViterbiInsideScores(curGrammar,
						level == startLevel);
				score = viScore[0][length][0];
			} else {
				doConstrainedInsideScores(curGrammar, viterbi, logScores);
				score = iScore[0][length][0][0];
			}
			if (score == Double.NEGATIVE_INFINITY)
				continue;
			// System.out.println("\nFound a parse for sentence with length "+length+". The LL is "+score+".");
			if (level < 1) {
				voScore[0][length][0] = 0.0;
				doConstrainedViterbiOutsideScores(curGrammar,
						level == startLevel);
			} else {
				oScore[0][length][0][0] = 0.0;
				doConstrainedOutsideScores(curGrammar, viterbi, logScores);
			}
			if (level ==3 ){
				//printChart(curGrammar, true);
				//System.exit(0);
			}
			pruneChart(
					/* Double.NEGATIVE_INFINITY */pruningThreshold[level + 1],
					curGrammar.numSubStates, level);
		}

	}
	

	protected void clearArrays() {
		iScore = oScore = null;
		viScore = voScore = null;
		allowedSubStates = null;
		vAllowedStates = null;
		words = null;
		vWords = null;
		narrowRExtent = wideRExtent = narrowLExtent = wideLExtent = null;
	}

	protected void createArrays(boolean firstTime, int numStates,
			short[] numSubStatesArray, int level, double initVal,
			boolean justInit) {
		if (firstTime) {
			viScore = new double[length][length + 1][];
			voScore = new double[length][length + 1][];

			iScore = new double[length][length + 1][][];
			oScore = new double[length][length + 1][][];

			allowedSubStates = new boolean[length][length + 1][][];
			allowedStates = new boolean[length][length + 1][];
			vAllowedStates = new boolean[length][length + 1];

			words = new int[length][length + 1][][];
			vWords = new int[length][length + 1][];
		}

		for (int start = 0; start < length; start++) {
			for (int end = start + 1; end <= length; end++) {
				if (firstTime) {
					viScore[start][end] = new double[numStates];
					voScore[start][end] = new double[numStates];
					iScore[start][end] = new double[numStates][];
					oScore[start][end] = new double[numStates][];

					allowedSubStates[start][end] = new boolean[numStates][];
					allowedStates[start][end] = new boolean[numStates];
					Arrays.fill(allowedStates[start][end], true);
					vAllowedStates[start][end] = true;
					
					words[start][end] = new int[numStates][];
					vWords[start][end] = new int[numStates];
				}
				for (int state = 0; state < numSubStatesArray.length; state++) {
					// if (end-start>1 && !grammarTags[state]) continue;
					/*
					 * if (refreshOnly){ if (allowedStates[start][end][state]){
					 * Arrays.fill(iScore[start][end][state], 0);
					 * Arrays.fill(oScore[start][end][state], 0); } continue; }
					 */

					if (firstTime || allowedStates[start][end][state]) {
						if (level < 1) {
							viScore[start][end][state] = Double.NEGATIVE_INFINITY;
							voScore[start][end][state] = Double.NEGATIVE_INFINITY;
						} else {
							iScore[start][end][state] = new double[numSubStatesArray[state]];
							oScore[start][end][state] = new double[numSubStatesArray[state]];
							Arrays.fill(iScore[start][end][state], initVal);
							Arrays.fill(oScore[start][end][state], initVal);
							words[start][end][state] = new int[numSubStatesArray[state]];
							Arrays.fill(words[start][end][state], -1);
							
							boolean[] newAllowedSubStates = new boolean[numSubStatesArray[state]];
							if (allowedSubStates[start][end][state] == null) {
								Arrays.fill(newAllowedSubStates, true);
								allowedSubStates[start][end][state] = newAllowedSubStates;
							} else {
								if (!justInit) {
									int[][] curLChildMap = lChildMap[level - 2];
									int[][] curRChildMap = rChildMap[level - 2];
									for (int i = 0; i < allowedSubStates[start][end][state].length; i++) {
										boolean val = allowedSubStates[start][end][state][i];
										newAllowedSubStates[curLChildMap[state][i]] = val;
										newAllowedSubStates[curRChildMap[state][i]] = val;
									}
									allowedSubStates[start][end][state] = newAllowedSubStates;
								}
							}
						}
					} else {
						if (level < 1) {
							viScore[start][end][state] = Double.NEGATIVE_INFINITY;
							voScore[start][end][state] = Double.NEGATIVE_INFINITY;
						} else {
							iScore[start][end][state] = null;
							oScore[start][end][state] = null;
							// allowedSubStates[start][end][state] = new
							// boolean[1];
							// allowedSubStates[start][end][state][0] = false;
						}
					}
				}
				if (level > 0 && start == 0 && end == length) {
					if (iScore[start][end][0] == null)
						System.out
								.println("ROOT does not span the entire tree!");
				}
			}
		}
		narrowRExtent = new int[length + 1][numStates];
		wideRExtent = new int[length + 1][numStates];
		narrowLExtent = new int[length + 1][numStates];
		wideLExtent = new int[length + 1][numStates];

		for (int loc = 0; loc <= length; loc++) {
			Arrays.fill(narrowLExtent[loc], -1); // the rightmost left with
													// state s ending at i that
													// we can get is the
													// beginning
			Arrays.fill(wideLExtent[loc], length + 1); // the leftmost left with
														// state s ending at i
														// that we can get is
														// the end
			Arrays.fill(narrowRExtent[loc], length + 1); // the leftmost right
															// with state s
															// starting at i
															// that we can get
															// is the end
			Arrays.fill(wideRExtent[loc], -1); // the rightmost right with state
												// s starting at i that we can
												// get is the beginning
		}
	}
	
	void initializeChart(Lattice nextLattice2, Lexicon lexicon,
			boolean noSubstates, boolean noSmoothing, List<String> posTags,
			boolean scale, boolean log) { //noSubState == false = problem?
		for (int beg = 0; beg != length; beg++)
		{
			for (Transition trans : nextLattice2.getTransPos(beg)) {
				String word = trans.word;
				int start = trans.beg;
				int end = trans.end;
				int goldTag = -1;

				if (useGoldPOS && posTags != null) {
					goldTag = tagNumberer.number(posTags.get(start));
				}
				for (int tag = 0; tag < numStates; tag++) {
					if (grammarTags[tag])
						continue;
					if (!noSubstates && !allowedStates[start][end][tag])
						continue;
					//if (grammarTags[tag])
					//	continue;
					if (useGoldPOS && posTags != null && tag != goldTag)
						continue;
					narrowRExtent[start][tag] = end;
					narrowLExtent[end][tag] = start;
					wideRExtent[start][tag] = end;
					wideLExtent[end][tag] = start;
					double[] lexiconScores = lexicon.score(word, (short) tag,
							start, noSmoothing, false, trans.prob, latticeWeight);
					for (short n = 0; n < lexiconScores.length; n++) {
						if (!noSubstates && !allowedSubStates[start][end][tag][n])
							continue;
						
						double prob = lexiconScores[n];
						//ROB HERE!
						/*if (log)
							prob += Math.log(trans.prob);
						else
							prob *= trans.prob;*/

						if (noSubstates)
						{
							if (prob < viScore[start][end][tag] || prob == Double.NEGATIVE_INFINITY)
								continue;
							viScore[start][end][tag] = prob;
							vWords[start][end][tag] = wordIds.getId(trans.word);
						}
						else
						{
							if (prob < iScore[start][end][tag][n] || prob == Double.NEGATIVE_INFINITY)
								continue;
							iScore[start][end][tag][n] = prob;
							words[start][end][tag][n] = wordIds.getId(trans.word);
						}
					}
				}
			}
		}
	}

	/**
	 * Fills in the iScore array of each category over each span of length 2 or
	 * more.
	 */
	void doConstrainedViterbiInsideScores(Grammar grammar, boolean level0grammar) {
		short[] numSubStatesArray = grammar.numSubStates;
		// double[] oldIScores = new double[maxNSubStates];
		// int smallestScale = 10, largestScale = -10;
		for (int diff = 1; diff <= length; diff++) {
			for (int start = 0; start < (length - diff + 1); start++) {
				int end = start + diff;
				final int lastState = (level0grammar) ? 1
						: numSubStatesArray.length;
				for (int pState = 0; pState < lastState; pState++) {
					if (diff == 1)
						continue; // there are no binary rules that span over 1
									// symbol only
					// if (iScore[start][end][pState] == null) { continue; }
					if (!grammarTags[pState])
						continue;
					if (!vAllowedStates[start][end])
						continue;
					double oldIScore = viScore[start][end][pState];
					double bestIScore = oldIScore;
					BinaryRule[] parentRules = grammar.splitRulesWithP(pState);
					for (int i = 0; i < parentRules.length; i++) {
						BinaryRule r = parentRules[i];
						int lState = r.leftChildState;
						int rState = r.rightChildState;

						int narrowR = narrowRExtent[start][lState];
						boolean iPossibleL = (narrowR < end); // can this left
																// constituent
																// leave space
																// for a right
																// constituent?
						if (!iPossibleL) {
							continue;
						}

						int narrowL = narrowLExtent[end][rState];
						boolean iPossibleR = (narrowL >= narrowR); // can this
																	// right
																	// constituent
																	// fit next
																	// to the
																	// left
																	// constituent?
						if (!iPossibleR) {
							continue;
						}

						int min1 = narrowR;
						int min2 = wideLExtent[end][rState];
						int min = (min1 > min2 ? min1 : min2); // can this right
																// constituent
																// stretch far
																// enough to
																// reach the
																// left
																// constituent?
						if (min > narrowL) {
							continue;
						}

						int max1 = wideRExtent[start][lState];
						int max2 = narrowL;
						int max = (max1 < max2 ? max1 : max2); // can this left
																// constituent
																// stretch far
																// enough to
																// reach the
																// right
																// constituent?
						if (min > max) {
							continue;
						}

						// new: loop over all substates
						double[][][] scores = r.getScores2();
						double pS = Double.NEGATIVE_INFINITY;
						if (scores[0][0] != null)
							pS = scores[0][0][0];
						if (pS == Double.NEGATIVE_INFINITY)
							continue;

						for (int split = min; split <= max; split++) {
							if (!vAllowedStates[start][split])
								continue;
							if (!vAllowedStates[split][end])
								continue;

							double lS = viScore[start][split][lState];
							if (lS == Double.NEGATIVE_INFINITY)
								continue;

							double rS = viScore[split][end][rState];
							if (rS == Double.NEGATIVE_INFINITY)
								continue;

							double tot = pS + lS + rS;
							if (tot >= bestIScore) {
								bestIScore = tot;
							}
						}
					}
					if (bestIScore > oldIScore) { // this way of making
													// "parentState" is better
						// than previous
						viScore[start][end][pState] = bestIScore;
						if (oldIScore == Double.NEGATIVE_INFINITY) {
							if (start > narrowLExtent[end][pState]) {
								narrowLExtent[end][pState] = start;
								wideLExtent[end][pState] = start;
							} else {
								if (start < wideLExtent[end][pState]) {
									wideLExtent[end][pState] = start;
								}
							}
							if (end < narrowRExtent[start][pState]) {
								narrowRExtent[start][pState] = end;
								wideRExtent[start][pState] = end;
							} else {
								if (end > wideRExtent[start][pState]) {
									wideRExtent[start][pState] = end;
								}
							}
						}
					}
				}
				final int lastStateU = (level0grammar && diff > 1) ? 1
						: numSubStatesArray.length;
				for (int pState = 0; pState < lastStateU; pState++) {
					//if (!grammarTags[pState]) //TODO can be put back?
					//	continue;
					// if (iScore[start][end][pState] == null) { continue; }
					// if (!allowedStates[start][end][pState][0]) continue;
					if (diff != 1 && !vAllowedStates[start][end])
						continue;

					UnaryRule[] unaries = grammar
							.getClosedViterbiUnaryRulesByParent(pState);
					double oldIScore = viScore[start][end][pState];
					double bestIScore = oldIScore;
					for (int r = 0; r < unaries.length; r++) {
						UnaryRule ur = unaries[r];
						int cState = ur.childState;

						if ((pState == cState))
							continue;// && (np == cp))continue;

						double iS = viScore[start][end][cState];
						if (iS == Double.NEGATIVE_INFINITY)
							continue;

						double[][] scores = ur.getScores2();
						double pS = Double.NEGATIVE_INFINITY;
						if (scores[0] != null)
							pS = scores[0][0];
						if (pS == Double.NEGATIVE_INFINITY)
							continue;

						double tot = iS + pS;

						if (tot >= bestIScore) {
							bestIScore = tot;
						}
					}
					if (bestIScore > oldIScore) {
						viScore[start][end][pState] = bestIScore;

						if (oldIScore == Double.NEGATIVE_INFINITY) {
							if (start > narrowLExtent[end][pState]) {
								narrowLExtent[end][pState] = start;
								wideLExtent[end][pState] = start;
							} else {
								if (start < wideLExtent[end][pState]) {
									wideLExtent[end][pState] = start;
								}
							}
							if (end < narrowRExtent[start][pState]) {
								narrowRExtent[start][pState] = end;
								wideRExtent[start][pState] = end;
							} else {
								if (end > wideRExtent[start][pState]) {
									wideRExtent[start][pState] = end;
								}
							}
						}
						// }
						// }
					}
				}
			}
		}
	}


	void doConstrainedViterbiOutsideScores(Grammar grammar,
			boolean level0grammar) {
		for (int diff = length; diff >= 1; diff--) {
			for (int start = 0; start + diff <= length; start++) {
				int end = start + diff;
				final int lastState = (level0grammar) ? 1 : numStates;
				for (int cState = 0; cState < lastState; cState++) {
					// if (diff>1 && !grammar.isGrammarTag[cState]) continue;
					if (!vAllowedStates[start][end])
						continue;

					double iS = viScore[start][end][cState];
					if (iS == Double.NEGATIVE_INFINITY) {
						continue;
					}

					double oldOScore = voScore[start][end][cState];
					double bestOScore = oldOScore;
					UnaryRule[] rules = grammar
							.getClosedViterbiUnaryRulesByChild(cState);
					for (int r = 0; r < rules.length; r++) {
						UnaryRule ur = rules[r];
						int pState = ur.parentState;
						if (cState == pState)
							continue;

						double oS = voScore[start][end][pState];
						if (oS == Double.NEGATIVE_INFINITY) {
							continue;
						}

						double[][] scores = ur.getScores2();

						double pS = scores[0][0];
						double tot = oS + pS;
						if (tot > bestOScore) {
							bestOScore = tot;
						}
					}
					if (bestOScore > oldOScore) {
						voScore[start][end][cState] = bestOScore;
					}

				}
				for (int pState = 0; pState < lastState; pState++) {
					if (!grammarTags[pState])
						continue;
					double oS = voScore[start][end][pState];
					if (oS == Double.NEGATIVE_INFINITY) {
						continue;
					}
					// if (!vAllowedStates[start][end]) continue;
					BinaryRule[] rules = grammar.splitRulesWithP(pState);
					for (int r = 0; r < rules.length; r++) {
						BinaryRule br = rules[r];

						int lState = br.leftChildState;
						int min1 = narrowRExtent[start][lState];
						if (end < min1) {
							continue;
						}

						int rState = br.rightChildState;
						int max1 = narrowLExtent[end][rState];
						if (max1 < min1) {
							continue;
						}

						int min = min1;
						int max = max1;
						if (max - min > 2) {
							int min2 = wideLExtent[end][rState];
							min = (min1 > min2 ? min1 : min2);
							if (max1 < min) {
								continue;
							}
							int max2 = wideRExtent[start][lState];
							max = (max1 < max2 ? max1 : max2);
							if (max < min) {
								continue;
							}
						}

						double[][][] scores = br.getScores2();
						double pS = Double.NEGATIVE_INFINITY;// scores[0][0][0];
						if (scores[0][0] != null)
							pS = scores[0][0][0];
						if (pS == Double.NEGATIVE_INFINITY) {
							continue;
						}

						for (int split = min; split <= max; split++) {
							if (!vAllowedStates[start][split])
								continue;
							if (!vAllowedStates[split][end])
								continue;

							double lS = viScore[start][split][lState];
							if (lS == Double.NEGATIVE_INFINITY) {
								continue;
							}

							double rS = viScore[split][end][rState];
							if (rS == Double.NEGATIVE_INFINITY) {
								continue;
							}

							double totL = pS + rS + oS;
							if (totL > voScore[start][split][lState]) {
								voScore[start][split][lState] = totL;
							}
							double totR = pS + lS + oS;
							if (totR > voScore[split][end][rState]) {
								voScore[split][end][rState] = totR;
							}
						}
					}
				}
			}
		}
	}
	
	void doConstrainedInsideScores(Grammar grammar, boolean viterbi,
			boolean logScores) {
		if (!viterbi && logScores)
			throw new Error(
					"This would require logAdds and is slow. Exponentiate the scores instead.");
		short[] numSubStatesArray = grammar.numSubStates;
		double initVal = (logScores) ? Double.NEGATIVE_INFINITY : 0;

		for (int diff = 1; diff <= length; diff++) {
			for (int start = 0; start < (length - diff + 1); start++) {
				int end = start + diff;
				for (int pState = 0; pState < numStates; pState++) {
					if (diff == 1)
						continue; // there are no binary rules that span over 1
									// symbol only
					if (!allowedStates[start][end][pState])
						continue;
					BinaryRule[] parentRules = grammar.splitRulesWithP(pState);
					final int nParentStates = numSubStatesArray[pState];
					Arrays.fill(scoresToAdd, initVal);
					boolean somethingChanged = false;
					final int numRules = parentRules.length;
					for (int i = 0; i < numRules; i++) {

						BinaryRule r = parentRules[i];
						int lState = r.leftChildState;
						int rState = r.rightChildState;

						int narrowR = narrowRExtent[start][lState];
						boolean iPossibleL = (narrowR < end); // can this left
																// constituent
																// leave space
																// for a right
																// constituent?
						if (!iPossibleL) {
							continue;
						}

						int narrowL = narrowLExtent[end][rState];
						boolean iPossibleR = (narrowL >= narrowR); // can this
																	// right
																	// constituent
																	// fit next
																	// to the
																	// left
																	// constituent?
						if (!iPossibleR) {
							continue;
						}

						int min1 = narrowR;
						int min2 = wideLExtent[end][rState];
						int min = (min1 > min2 ? min1 : min2); // can this right
																// constituent
																// stretch far
																// enough to
																// reach the
																// left
																// constituent?
						if (min > narrowL) {
							continue;
						}

						int max1 = wideRExtent[start][lState];
						int max2 = narrowL;
						final int max = (max1 < max2 ? max1 : max2); // can this
																		// left
																		// constituent
																		// stretch
																		// far
																		// enough
																		// to
																		// reach
																		// the
																		// right
																		// constituent?
						if (min > max) {
							continue;
						}

						// TODO switch order of loops for efficiency
						double[][][] scores = r.getScores2();
						final int nLeftChildStates = numSubStatesArray[lState];
						final int nRightChildStates = numSubStatesArray[rState];
						for (int split = min; split <= max; split++) {
							if (!allowedStates[start][split][lState])
								continue;
							if (!allowedStates[split][end][rState])
								continue;
							for (int lp = 0; lp < nLeftChildStates; lp++) {
								// if (iScore[start][split][lState] == null)
								// continue;
								// if
								// (!allowedSubStates[start][split][lState][lp])
								// continue;
								double lS = iScore[start][split][lState][lp];
								if (lS == initVal)
									continue;

								for (int rp = 0; rp < nRightChildStates; rp++) {
									if (scores[lp][rp] == null)
										continue;
									double rS = iScore[split][end][rState][rp];
									if (rS == initVal)
										continue;
									for (int np = 0; np < nParentStates; np++) {
										//if (start == 0 && end == 2 && np == 8 && lp == 11 && rp == 0 && 
										//		idToString(pState).equals("NP^g") && idToString(rState).equals("NNP") &&
										//		idToString(lState).equals("NP^g"))
										//	System.out.println("Found");
										
										if (!allowedSubStates[start][end][pState][np])
											continue;
										// if (level==endLevel-1)
										// edgesTouched++;

										double pS = scores[lp][rp][np];
										//if (start == 0 && end == 2 && np == 8 && lp == 11 && rp == 0 && 
										//		idToString(pState).equals("NP^g") && idToString(rState).equals("NNP") &&
										//		idToString(lState).equals("NP^g"))
										//{
										//	System.out.println("Found2");
										//	System.out.println(pS + "  " + lS + "  " + rS);
										//}

										if (pS == initVal)
											continue;

										double thisRound = (logScores) ? pS
												+ lS + rS : pS * lS * rS;

										if (viterbi)
											scoresToAdd[np] = Math.max(
													thisRound, scoresToAdd[np]);
										else
											scoresToAdd[np] += thisRound;
										//if (!viterbi)
										//	System.out.println(start + "," + end + idToString(pState) + "_" + np 
										//			+ "\t" + idToString(rState) + "_" + rp + "  " + idToString(lState) + 
										//			"_" + lp + "\t" + scoresToAdd[np]);
										somethingChanged = true;
									}
								}
							}
						}
					}
					if (!somethingChanged)
						continue;

					for (int np = 0; np < nParentStates; np++) {
						if (scoresToAdd[np] > initVal) {
							iScore[start][end][pState][np] = scoresToAdd[np];
						}
					}
					if (true) {// firstTime) {
						if (start > narrowLExtent[end][pState]) {
							narrowLExtent[end][pState] = start;
							wideLExtent[end][pState] = start;
						} else {
							if (start < wideLExtent[end][pState]) {
								wideLExtent[end][pState] = start;
							}
						}
						if (end < narrowRExtent[start][pState]) {
							narrowRExtent[start][pState] = end;
							wideRExtent[start][pState] = end;
						} else {
							if (end > wideRExtent[start][pState]) {
								wideRExtent[start][pState] = end;
							}
						}
					}
				}
				double[][] scoresAfterUnaries = new double[numStates][];
				boolean somethingChanged = false;
				for (int pState = 0; pState < numStates; pState++) {
					if (!allowedStates[start][end][pState])
						continue;

					// Should be: Closure under sum-product:
					UnaryRule[] unaries = null;
					if (viterbi)
						unaries = grammar
								.getClosedViterbiUnaryRulesByParent(pState);
					else
						unaries = grammar
								.getClosedSumUnaryRulesByParent(pState);
					final int nParentStates = numSubStatesArray[pState];// scores[0].length;
					boolean firstTime = true;
					final int numRules = unaries.length;
					for (int r = 0; r < numRules; r++) {
						UnaryRule ur = unaries[r];
						int cState = ur.childState;
						if ((pState == cState))
							continue;// && (np == cp))continue;
						if (iScore[start][end][cState] == null)
							continue;
						double[][] scores = ur.getScores2();
						final int nChildStates = numSubStatesArray[cState];// scores[0].length;
						for (int cp = 0; cp < nChildStates; cp++) {
							if (scores[cp] == null)
								continue;
							for (int np = 0; np < nParentStates; np++) {
								if (!allowedSubStates[start][end][pState][np])
									continue;
								// if
								// (!allowedSubStates[start][end][cState][cp])
								// continue;
								// if (level==endLevel-1) edgesTouched++;

								double pS = scores[cp][np];
								if (pS == initVal)
									continue;

								double iS = iScore[start][end][cState][cp];
								if (iS == initVal)
									continue;

								if (firstTime) {
									firstTime = false;
									scoresAfterUnaries[pState] = new double[nParentStates];
									Arrays.fill(scoresAfterUnaries[pState],
											initVal);

								}
								double thisRound = (logScores) ? iS + pS : iS
										* pS;

								if (viterbi)
									scoresAfterUnaries[pState][np] = Math.max(
											thisRound,
											scoresAfterUnaries[pState][np]);
								else
									scoresAfterUnaries[pState][np] += thisRound;
								somethingChanged = true;
								//if (!viterbi)
								//	System.out.println("un" + " " + start + "," + end + "  " + idToString(pState) + "_" + np + " -> " + idToString(cState) +"_" + cp + "\t"+
								//			scoresAfterUnaries[pState][np] + "\t" + iS + "\t" + pS);

							}
						}
					}
				}
				if (!somethingChanged)
					continue;
				for (int pState = 0; pState < numStates; pState++) {
					final int nParentStates = numSubStatesArray[pState];
					double[] thisCell = scoresAfterUnaries[pState];
					if (thisCell == null)
						continue;
					for (int np = 0; np < nParentStates; np++) {
						if (thisCell[np] > initVal) {
							if (viterbi)
								iScore[start][end][pState][np] = Math.max(
										iScore[start][end][pState][np],
										thisCell[np]);
							else
								iScore[start][end][pState][np] = iScore[start][end][pState][np]
										+ thisCell[np];
						}
					}
					if (true) {
						if (start > narrowLExtent[end][pState]) {
							narrowLExtent[end][pState] = start;
							wideLExtent[end][pState] = start;
						} else {
							if (start < wideLExtent[end][pState]) {
								wideLExtent[end][pState] = start;
							}
						}
						if (end < narrowRExtent[start][pState]) {
							narrowRExtent[start][pState] = end;
							wideRExtent[start][pState] = end;
						} else {
							if (end > wideRExtent[start][pState]) {
								wideRExtent[start][pState] = end;
							}
						}
					}
				}
			}
		}
	}


	/**
	 * Fills in the oScore array of each category over each span of length 2 or
	 * more. This version computes the posterior outside scores, not the Viterbi
	 * outside scores.
	 */

	void doConstrainedOutsideScores(Grammar grammar, boolean viterbi,
			boolean logScores) {
		short[] numSubStatesArray = grammar.numSubStates;
		double initVal = (logScores) ? Double.NEGATIVE_INFINITY : 0.0;
		for (int diff = length; diff >= 1; diff--) {
			for (int start = 0; start + diff <= length; start++) {
				int end = start + diff;
				// do unaries
				double[][] scoresAfterUnaries = new double[numStates][];
				boolean somethingChanged = false;
				for (int cState = 0; cState < numStates; cState++) {
					//if (!grammar.isGrammarTag[cState])
					//	continue;// Rob: can this be replaced?
					if (oScore[start][end][cState] == null) {
						continue;
					}
					UnaryRule[] rules = null;
					if (viterbi)
						rules = grammar
								.getClosedViterbiUnaryRulesByChild(cState);
					else
						rules = grammar.getClosedSumUnaryRulesByChild(cState);
					final int nChildStates = numSubStatesArray[cState];
					final int numRules = rules.length;
					for (int r = 0; r < numRules; r++) {
						UnaryRule ur = rules[r];
						int pState = ur.parentState;
						if ((pState == cState))
							continue;// && (np == cp))continue;
						if (oScore[start][end][pState] == null) {
							continue;
						}

						double[][] scores = ur.getScores2();
						final int nParentStates = numSubStatesArray[pState];
						for (int cp = 0; cp < nChildStates; cp++) {
							if (scores[cp] == null)
								continue;
							if (!allowedSubStates[start][end][cState][cp])
								continue;
							for (int np = 0; np < nParentStates; np++) {
								double pS = scores[cp][np];
								if (pS == initVal)
									continue;

								double oS = oScore[start][end][pState][np];
								if (oS == initVal)
									continue;

								double thisRound = (logScores) ? oS + pS : oS
										* pS;

								if (scoresAfterUnaries[cState] == null) {
									scoresAfterUnaries[cState] = new double[numSubStatesArray[cState]];
									if (viterbi)
										Arrays.fill(scoresAfterUnaries[cState],
												initVal);
								}

								if (viterbi)
									scoresAfterUnaries[cState][cp] = Math.max(
											thisRound,
											scoresAfterUnaries[cState][cp]);
								else
									scoresAfterUnaries[cState][cp] += thisRound;
								somethingChanged = true;
							}
						}
					}
				}
				if (somethingChanged) {
					for (int cState = 0; cState < numStates; cState++) {
						double[] thisCell = scoresAfterUnaries[cState];
						if (thisCell == null)
							continue;
						for (int cp = 0; cp < numSubStatesArray[cState]; cp++) {
							if (thisCell[cp] > initVal) {
								if (viterbi)
									oScore[start][end][cState][cp] = Math.max(
											oScore[start][end][cState][cp],
											thisCell[cp]);
								else
									oScore[start][end][cState][cp] += thisCell[cp];
							}
						}
					}
				}

				// do binaries

				for (int pState = 0; pState < numSubStatesArray.length; pState++) {
					if (oScore[start][end][pState] == null) {
						continue;
					}
					final int nParentChildStates = numSubStatesArray[pState];
					// if (!allowedStates[start][end][pState]) continue;
					BinaryRule[] rules = grammar.splitRulesWithP(pState);
					final int numRules = rules.length;
					for (int r = 0; r < numRules; r++) {
						BinaryRule br = rules[r];
						int lState = br.leftChildState;
						int min1 = narrowRExtent[start][lState];
						if (end < min1) {
							continue;
						}

						int rState = br.rightChildState;
						int max1 = narrowLExtent[end][rState];
						if (max1 < min1) {
							continue;
						}

						int min = min1;
						int max = max1;
						if (max - min > 2) {
							int min2 = wideLExtent[end][rState];
							min = (min1 > min2 ? min1 : min2);
							if (max1 < min) {
								continue;
							}
							int max2 = wideRExtent[start][lState];
							max = (max1 < max2 ? max1 : max2);
							if (max < min) {
								continue;
							}
						}

						double[][][] scores = br.getScores2();
						final int nLeftChildStates = numSubStatesArray[lState];
						final int nRightChildStates = numSubStatesArray[rState];
						for (int split = min; split <= max; split++) {
							if (oScore[start][split][lState] == null)
								continue;
							if (oScore[split][end][rState] == null)
								continue;
							// if (!allowedStates[start][split][lState])
							// continue;
							// if (!allowedStates[split][end][rState]) continue;
							double[] rightScores = new double[nRightChildStates];
							if (viterbi)
								Arrays.fill(rightScores, initVal);
							Arrays.fill(scoresToAdd, initVal);
							somethingChanged = false;
							for (int lp = 0; lp < nLeftChildStates; lp++) {
								double lS = iScore[start][split][lState][lp];
								if (lS == initVal) {
									continue;
								}
								// if
								// (!allowedSubStates[start][split][lState][lp])
								// continue;
								for (int rp = 0; rp < nRightChildStates; rp++) {
									if (scores[lp][rp] == null)
										continue;
									double rS = iScore[split][end][rState][rp];
									if (rS == initVal) {
										continue;
									}
									// if
									// (!allowedSubStates[split][end][rState][rp])
									// continue;

									for (int np = 0; np < nParentChildStates; np++) {
										double pS = scores[lp][rp][np];
										if (pS == initVal)
											continue;

										double oS = oScore[start][end][pState][np];
										if (oS == initVal)
											continue;

										double thisRoundL = (logScores) ? pS
												+ rS + oS : pS * rS * oS;
										double thisRoundR = (logScores) ? pS
												+ lS + oS : pS * lS * oS;

										if (viterbi) {
											scoresToAdd[lp] = Math
													.max(thisRoundL,
															scoresToAdd[lp]);
											rightScores[rp] = Math
													.max(thisRoundR,
															rightScores[rp]);
										} else {
											scoresToAdd[lp] += thisRoundL;
											rightScores[rp] += thisRoundR;
										}

										somethingChanged = true;
									}
								}
							}
							if (!somethingChanged)
								continue;
							for (int cp = 0; cp < nLeftChildStates; cp++) {
								if (scoresToAdd[cp] > initVal) {
									if (viterbi)
										oScore[start][split][lState][cp] = Math
												.max(oScore[start][split][lState][cp],
														scoresToAdd[cp]);
									else
										oScore[start][split][lState][cp] += scoresToAdd[cp];
								}
							}

							for (int cp = 0; cp < nRightChildStates; cp++) {
								if (rightScores[cp] > initVal) {
									if (viterbi)
										oScore[split][end][rState][cp] = Math
												.max(oScore[split][end][rState][cp],
														rightScores[cp]);
									else
										oScore[split][end][rState][cp] += rightScores[cp];
								}
							}
						}
					}
				}
			}
		}
	}


	protected void pruneChart(double threshold, short[] numSubStatesArray,
			int level) {
		@SuppressWarnings("unused")
		int totalStates = 0, previouslyPossible = 0, nowPossible = 0;
		// threshold = Double.NEGATIVE_INFINITY;

		double sentenceProb = (level < 1) ? viScore[0][length][0]
				: iScore[0][length][0][0];
		// double sentenceScale = iScale[0][length][0];//+1.0 for oScale
		if (level < 1)
			nowPossible = totalStates = previouslyPossible = length;
		int startDiff = (level < 0) ? 2 : 1;
		for (int diff = startDiff; diff <= length; diff++) {
			for (int start = 0; start < (length - diff + 1); start++) {
				int end = start + diff;
				int lastState = (level < 0) ? 1 : numSubStatesArray.length;
				for (int state = 0; state < lastState; state++) {
					//if (diff > 1 && !grammarTags[state])
					//	continue;
					// boolean allFalse = true;
					if (state == 0) {
						allowedStates[start][end][state] = true;
						// if (level>1){
						// allowedSubStates[start][end][state] = new boolean[1];
						// allowedSubStates[start][end][state][0] = true;
						// }
						continue;
					}
					if (level == 0) {
						if (!vAllowedStates[start][end]) {

							allowedStates[start][end][state] = false;
							totalStates++;
							continue;
						}
					} else if (level > 0) {
						if (!allowedStates[start][end][state]) {
							totalStates += numSubStatesArray[state];
							continue;
						}
					}

					if (level < 1) {
						totalStates++;
						previouslyPossible++;
						double iS = viScore[start][end][state];
						double oS = voScore[start][end][state];
						if (iS == Double.NEGATIVE_INFINITY
								|| oS == Double.NEGATIVE_INFINITY) {
							if (level == 0)
								allowedStates[start][end][state] = false;
							else
								/* level==-1 */vAllowedStates[start][end] = false;
							continue;
						}


						double posterior = iS + oS - sentenceProb;

						if (posterior > threshold) {

							// spanMass[start][end]+=Math.exp(posterior);
							if (level == 0)
								allowedStates[start][end][state] = true;
							else
								vAllowedStates[start][end] = true;
							nowPossible++;
						} else {
							
							if (level == 0)
								allowedStates[start][end][state] = false;
							else
								vAllowedStates[start][end] = false;
						}
						continue;
					}

					// level >= 1 -> iterate over substates
					boolean nonePossible = true;

					for (int substate = 0; substate < numSubStatesArray[state]; substate++) {
						totalStates++;
						if (!allowedSubStates[start][end][state][substate])
							continue;
						previouslyPossible++;
						double iS = iScore[start][end][state][substate];
						double oS = oScore[start][end][state][substate];

						//String stateStr = idToString(state).trim();
						//if (start  == 1 && end == 3 && stateStr.equals("PRP"))
							//System.out.println(start + "," + end + '\t' + stateStr + "_" + substate + 
							//				"  " + iS + "  " + oS);
						if (iS == Double.NEGATIVE_INFINITY
								|| oS == Double.NEGATIVE_INFINITY) {
							allowedSubStates[start][end][state][substate] = false;
							continue;
						}
						double posterior = iS + oS - sentenceProb;

						if (posterior > threshold) {


							allowedSubStates[start][end][state][substate] = true;
							nowPossible++;
							//System.out.println(start + "," + end + "  " + idToString(state) + "_" + substate +'\t' + posterior);
							// spanMass[start][end]+=Math.exp(posterior);
							nonePossible = false;
						} else {
							allowedSubStates[start][end][state][substate] = false;
						}

					}
					if (nonePossible)
						allowedStates[start][end][state] = false;
				}
			}
		}
	}
	
	/**
	 * Assumes that inside and outside scores (sum version, not viterbi) have
	 * been computed. In particular, the narrowRExtent and other arrays need not
	 * be updated.
	 */
	void doConstrainedMaxCScores(Lattice nextLattice2, Grammar grammar,
			Lexicon lexicon, final boolean scale) {
		numSubStatesArray = grammar.numSubStates;
		double initVal = Double.NEGATIVE_INFINITY;
		chartBeforeU = new LazyList[length][length + 1][numStates];
		chartAfterU = new LazyList[length][length + 1][numStates];
		double logNormalizer = iScore[0][length][0][0];
		// double thresh2 = threshold*logNormalizer;
		for (int diff = 1; diff <= length; diff++) {
			// System.out.print(diff + " ");
			for (int start = 0; start < (length - diff + 1); start++) {
				int end = start + diff;
				if (diff > 1) {
					// diff > 1: Try binary rules
					for (int pState = 0; pState < numSubStatesArray.length; pState++) {
						if (!allowedStates[start][end][pState])
							continue;
						chartBeforeU[start][end][pState] = new LazyList(
								grammar.isGrammarTag);
						BinaryRule[] parentRules = grammar
								.splitRulesWithP(pState);
						int nParentStates = numSubStatesArray[pState]; // ==
																		// scores[0][0].length;
						double bestScore = Double.NEGATIVE_INFINITY;
						HyperEdge bestElement = null;

						for (int i = 0; i < parentRules.length; i++) {
							BinaryRule r = parentRules[i];
							int lState = r.leftChildState;
							int rState = r.rightChildState;

							int narrowR = narrowRExtent[start][lState];
							boolean iPossibleL = (narrowR < end); 
							// can this left constituent leave space for a right constituent?
							if (!iPossibleL)
								continue;

							int narrowL = narrowLExtent[end][rState];
							boolean iPossibleR = (narrowL >= narrowR); 
							// can this right constituent fit next to the left constituent?
							if (!iPossibleR)
								continue;

							int min1 = narrowR;
							int min2 = wideLExtent[end][rState];
							int min = (min1 > min2 ? min1 : min2); 
							// can this right constituent stretch far enough to reach the left constituent?
							if (min > narrowL) 
								continue;

							int max1 = wideRExtent[start][lState];
							int max2 = narrowL;
							int max = (max1 < max2 ? max1 : max2); 
							// can this left constituent stretch far enough to reach the right constituent?
							if (min > max) 
								continue;

							double[][][] scores = r.getScores2();
							int nLeftChildStates = numSubStatesArray[lState]; // ==
																				// scores.length;
							int nRightChildStates = numSubStatesArray[rState]; // ==
																				// scores[0].length;
							for (int split = min; split <= max; split++) {
								double ruleScore = 0;
								if (!allowedStates[start][split][lState])
									continue;
								if (!allowedStates[split][end][rState])
									continue;

								HyperEdge bestLeft = chartAfterU[start][split][lState]
										.getKbest(0);
								double leftChildScore = (bestLeft == null) ? Double.NEGATIVE_INFINITY
										: bestLeft.score;

								HyperEdge bestRight = chartAfterU[split][end][rState]
										.getKbest(0);
								double rightChildScore = (bestRight == null) ? Double.NEGATIVE_INFINITY
										: bestRight.score;

								if (leftChildScore == initVal
										|| rightChildScore == initVal)
									continue;

								double scalingFactor = 0.0;
								double gScore = leftChildScore + scalingFactor
										+ rightChildScore;

								if (gScore == Double.NEGATIVE_INFINITY)
									continue; // no chance of finding a better
												// derivation

								for (int lp = 0; lp < nLeftChildStates; lp++) {
									double lIS = iScore[start][split][lState][lp];
									if (lIS == 0)
										continue;

									for (int rp = 0; rp < nRightChildStates; rp++) {
										if (scores[lp][rp] == null)
											continue;
										double rIS = iScore[split][end][rState][rp];
										if (rIS == 0)
											continue;
										for (int np = 0; np < nParentStates; np++) {
											// if
											// (!allowedSubStates[start][end][pState][np])
											// continue;
											double pOS = oScore[start][end][pState][np];
											if (pOS == 0)
												continue;
											// if (pOS < thresh2) continue;

											double ruleS = scores[lp][rp][np];
											if (ruleS == 0)
												continue;
											ruleScore += (pOS * ruleS * lIS * rIS)
													/ logNormalizer;
										}
									}
								}
								if (ruleScore == 0)
									continue;

								ruleScore = Math.log(ruleScore);
								gScore += ruleScore;

								if (gScore > Double.NEGATIVE_INFINITY) {
									HyperEdge newElement = new HyperEdge(
											pState, lState, rState, 0, 0, 0,
											start, split, end, gScore,
											ruleScore);
									if (gScore > bestScore) {
										bestScore = gScore;
										bestElement = newElement;
									}
									if (diff > 2)
									{
										chartBeforeU[start][end][pState]
												.addToFringe(newElement);
										//System.out.println("Binary: "+ start + "," + end + "\t" + idToString(pState) + " -> " + 
										//		idToString(lState) + ' ' + idToString(rState) + "\t" + newElement.score);										
									}
								}
							}
						}
						if (diff == 2 && bestElement != null)
						{
							chartBeforeU[start][end][pState]
									.addToFringe(bestElement);
						}
						// chartBeforeU[start][end][pState].expandNextBest();
					}
				}
				// We treat TAG --> word exactly as if it was a unary rule,
				// except the score of the rule is
				// given by the lexicon rather than the grammar and that we
				// allow another unary on top of it.
				//TODO should be done in own function initCChart();
				for (int tag = 0; tag < numSubStatesArray.length; tag++) {
					if (!allowedStates[start][end][tag])
						continue;
					if (grammarTags[tag])
						continue; 
					chartBeforeU[start][end][tag] = new LazyList(
							grammar.isGrammarTag);
					int nTagStates = numSubStatesArray[tag];
					
					for(Transition trans : nextLattice2.getTransitions(start, end))
					{
						if (trans== null)
							continue;
						if (grammar.isGrammarTag(tag))
							continue;
						double[] lexiconScoreArray = lexicon.score(trans.word,
								(short) tag, start, false, false, trans.prob, latticeWeight);
						double lexiconScores = 0;
						for (int tp = 0; tp < nTagStates; tp++) {
							double pOS = oScore[start][end][tag][tp];
							double ruleS = lexiconScoreArray[tp];
							lexiconScores += (pOS * ruleS) / logNormalizer; 
							// The inside score of a word is 0.0f
						}
						double scalingFactor = 0.0;
							lexiconScores = Math.log(lexiconScores);
						double gScore = lexiconScores + scalingFactor;
						//ROB HERE!
						//gScore += Math.log(trans.prob);
						
						HyperEdge newElement = new HyperEdge(tag, -1, -1, 0, 0,
								0, start, start, end, gScore, lexiconScores);
						newElement.addWord(wordIds.getId(trans.word));
						chartBeforeU[start][end][tag].addToFringe(newElement);

						//System.out.println("tagging: "+ start + "," + end + "\t" + idToString(tag) +" ->" +word + "\t" + newElement.score);
					}
				}
				
				for (int pState = 0; pState < numSubStatesArray.length; pState++) {
					if (!allowedStates[start][end][pState])
						continue;
					chartAfterU[start][end][pState] = new LazyList(
							grammar.isGrammarTag);
					int nParentStates = numSubStatesArray[pState]; 
					UnaryRule[] unaries = grammar
							.getClosedSumUnaryRulesByParent(pState);
					HyperEdge bestElement = null;
					double bestScore = Double.NEGATIVE_INFINITY;

					for (int r = 0; r < unaries.length; r++) {
						UnaryRule ur = unaries[r];
						int cState = ur.childState;
						if ((pState == cState))
							continue;
						if (iScore[start][end][cState] == null)
							continue;

						double childScore = Double.NEGATIVE_INFINITY;
						if (chartBeforeU[start][end][cState] != null) {
							HyperEdge bestChild = chartBeforeU[start][end][cState]
									.getKbest(0);
							childScore = (bestChild == null) ? Double.NEGATIVE_INFINITY
									: bestChild.score;
						}

						if (childScore == initVal)
							continue;

						double scalingFactor = 0.0;
						double gScore = scalingFactor + childScore;

						double[][] scores = ur.getScores2();
						int nChildStates = numSubStatesArray[cState]; // ==
																		// scores.length;
						double ruleScore = 0;
						for (int cp = 0; cp < nChildStates; cp++) {
							double cIS = iScore[start][end][cState][cp];
							if (cIS == 0)
								continue;

							if (scores[cp] == null)
								continue;
							for (int np = 0; np < nParentStates; np++) {

								double pOS = oScore[start][end][pState][np];
								if (pOS < 0)
									continue;

								double ruleS = scores[cp][np];
								if (ruleS == 0)
									continue;
								ruleScore += (pOS * ruleS * cIS)
										/ logNormalizer;
							}
						}
						if (ruleScore == 0)
							continue;

						ruleScore = Math.log(ruleScore);
						gScore += ruleScore;

						if (gScore > Double.NEGATIVE_INFINITY) {
							HyperEdge newElement = new HyperEdge(pState,
									cState, 0, 0, start, end, gScore, ruleScore);
							if (gScore > bestScore) {
								bestScore = gScore;
								bestElement = newElement;
							}
							if (diff > 1)
								chartAfterU[start][end][pState]
										.addToFringe(newElement);
							//System.out.println("unary: "+ start + "," + end + "\t" + idToString(pState) + " -> " 
							//			+ idToString(cState) + "\t" + newElement.score);
						}
					}

					if (diff == 1 && bestElement != null)
						chartAfterU[start][end][pState]
								.addToFringe(bestElement);
					if (chartBeforeU[start][end][pState] != null) {
						HyperEdge bestSelf = chartBeforeU[start][end][pState]
								.getKbest(0);
						if (bestSelf != null) {
							HyperEdge selfRule = new HyperEdge(pState, pState,
									0, 0, start, end, bestSelf.score, 0);
							chartAfterU[start][end][pState]
									.addToFringe(selfRule);
						}
					}
				}
			}
		}
	}

	/**
	 * Returns the best parse, the one with maximum expected labelled recall.
	 * Assumes that the maxc* arrays have been filled.
	 */
	public Tree<String> extractBestMaxRuleParse(int start, int end,
			Lattice lattice) {
		return extractBestMaxRuleParse1(start, end, 0, 0, lattice);
	}

	public List<Tree<String>> extractKBestMaxRuleParses(int start, int end,
			Lattice nextLattice2, int k) {
		List<Tree<String>> list = new ArrayList<Tree<String>>(k);
		maxRuleScores = new ArrayList<Double>(k);
		for (int i = 0; i < k; i++) {
			Tree<String> tmp = extractBestMaxRuleParse1(start, end, 0, i,
					nextLattice2);
			if (tmp != null) {
				maxRuleScores.add(chartAfterU[0][length][0].getKbest(i).score);
			}
			// HyperEdge parentNode = chartAfterU[start][end][0].getKbest(i);
			// if (parentNode!=null) System.out.println(parentNode.score+" ");
			if (tmp != null)
				list.add(tmp);
			else
				break;
		}
		return list;
	}

	/**
	 * Returns the best parse for state "state", potentially starting with a
	 * unary rule
	 */
	public Tree<String> extractBestMaxRuleParse1(int start, int end, int state,
			int suboptimalities, Lattice lattice) {
		
		HyperEdge parentNode = chartAfterU[start][end][state]
				.getKbest(suboptimalities);
		if (parentNode == null) {
			//System.err.println("Don't have a " + (suboptimalities + 1)
			//		+ "-best tree.");
			return null;
		}
		int cState = parentNode.childState;
		Tree<String> result = null;

		HyperEdge childNode = chartBeforeU[start][end][cState]
				.getKbest(parentNode.childBest);

		List<Tree<String>> children = new ArrayList<Tree<String>>();
		String stateStr = (String) tagNumberer.object(cState);// +""+start+""+end;
		if (stateStr.endsWith("^g"))
			stateStr = stateStr.substring(0, stateStr.length() - 2);

		if (!grammarTags[cState]) {
			children.add(new Tree<String>(wordIds.getWord(childNode.getWord())));
		} else {
			int split = childNode.split;
			if (split == -1) {
				System.err
						.println("Warning: no symbol can generate the span from "
								+ start + " to " + end + ".");
				System.err.println("The insideScores are "
						+ Arrays.toString(iScore[start][end][state])
						+ " and the outsideScores are "
						+ Arrays.toString(oScore[start][end][state]));
				// return extractBestMaxRuleParse2(start, end,
				// maxcChild[start][end][state], sentence);
				return new Tree<String>("ROOT");
			}
			int lState = childNode.lChildState;
			int rState = childNode.rChildState;
			Tree<String> leftChildTree = extractBestMaxRuleParse1(start, split,
					lState, childNode.lChildBest, lattice);
			Tree<String> rightChildTree = extractBestMaxRuleParse1(split, end,
					rState, childNode.rChildBest, lattice);
			children.add(leftChildTree);
			children.add(rightChildTree);
		}

		boolean scale = false;
		updateConstrainedMaxCScores(lattice, scale, childNode);

		result = new Tree<String>(stateStr, children);
		if (cState != state) { // unaryRule
			stateStr = (String) tagNumberer.object(state);// +""+start+""+end;
			if (stateStr.endsWith("^g"))
				stateStr = stateStr.substring(0, stateStr.length() - 2);

			int intermediateNode = grammar.getUnaryIntermediate((short) state,
					(short) cState);
			if (intermediateNode > 0) {
				List<Tree<String>> restoredChild = new ArrayList<Tree<String>>();
				String stateStr2 = (String) tagNumberer
						.object(intermediateNode);
				if (stateStr2.endsWith("^g"))
					stateStr2 = stateStr2.substring(0, stateStr2.length() - 2);
				restoredChild.add(result);
				result = new Tree<String>(stateStr2, restoredChild);
			}
			List<Tree<String>> childs = new ArrayList<Tree<String>>();
			childs.add(result);
			result = new Tree<String>(stateStr, childs);
		}
		updateConstrainedMaxCScores(lattice, scale, parentNode);

		return result;
	}
	
	public void initCascade(Grammar gr, Lexicon lex) {
		// the cascades will contain all the projections (in logarithm mode) and
		// at the end the final grammar,
		// once in logarithm-mode and once not
		for (int level = startLevel; level <= endLevel + 1; level++) {
			if (level == -1)
				continue; // don't do the pre-pre parse
			Grammar tmpGrammar = null;
			Lexicon tmpLexicon = null;
			if (level == endLevel) {
				tmpGrammar = gr.copyGrammar(false);
				tmpLexicon = lex.copyLexicon();
			} else if (level > endLevel) {
				tmpGrammar = gr;
				tmpLexicon = lex;
			} else /* if (level>0&& level<endLevel) */{
				int[][] fromMapping = gr.computeMapping(1);
				int[][] toSubstateMapping = gr.computeSubstateMapping(level);
				int[][] toMapping = gr.computeToMapping(level,
						toSubstateMapping);
				int[][] curLChildMap = new int[toSubstateMapping.length][];
				int[][] curRChildMap = new int[toSubstateMapping.length][];
				double[] condProbs = gr.computeConditionalProbabilities(
						fromMapping, toMapping);

				if (level == -1)
					tmpGrammar = gr.projectTo0LevelGrammar(condProbs,
							fromMapping, toMapping);
				else
					tmpGrammar = gr.projectGrammar(condProbs, fromMapping,
							toSubstateMapping);
				tmpLexicon = lex.projectLexicon(condProbs, fromMapping,
						toSubstateMapping);

				if (level > 0) {
					lChildMap[level + startLevel] = curLChildMap;
					rChildMap[level + startLevel] = curRChildMap;
					gr.computeReverseSubstateMapping(level, curLChildMap,
							curRChildMap);
				}
			}

			tmpGrammar.splitRules();
			double filter = 1.0e-4;
			if (level >= 0 && level < endLevel) {
				tmpGrammar.removeUnlikelyRules(filter, 1.0);
				tmpLexicon.removeUnlikelyTags(filter, 1.0);
			} else if (level >= endLevel) {
				tmpGrammar.removeUnlikelyRules(1.0e-10, 1.0);
				tmpLexicon.removeUnlikelyTags(1.0e-10, 1.0);
			}
			// System.out.println(baseGrammar.toString());

			// DumpGrammar.dumpGrammar("wsj_"+level+".gr", tmpGrammar,
			// (SophisticatedLexicon)tmpLexicon);

			if (level <= endLevel || viterbiParse) {
				tmpGrammar.logarithmMode();
				tmpLexicon.logarithmMode();
			}
			grammarCascade[level - startLevel] = tmpGrammar;
			lexiconCascade[level - startLevel] = tmpLexicon;

		}
	}


	// belongs in the grammar but i didnt want to change the signature for
	// now...
	public int maxSubStates(Grammar grammar) {
		int max = 0;
		for (int i = 0; i < numStates; i++) {
			if (grammar.numSubStates[i] > max)
				max = grammar.numSubStates[i];
		}
		return max;
	}
	
	public void setID(int i, PriorityQueue<List<Tree<String>>> q) {
		myID = i;
		queue = q;
	}
	
	public void setNextSentence(Lattice lattice, int nextID) {
		nextLattice = lattice;
		nextSentenceID = nextID;
	}

	void updateConstrainedMaxCScores(Lattice nextLattice2,
			final boolean scale, HyperEdge parent) {
		int start = parent.start;
		int end = parent.end;
		int pState = parent.parentState;
		int suboptimalities = parent.parentBest + 1;
		double ruleScore = parent.ruleScore;

		if (parent.alreadyExpanded)
			return;

		if (!parent.isUnary) {
			// if (chartBeforeU[start][end][pState].sortedListSize() >=
			// suboptimalities) return; // already have enough derivations

			int lState = parent.lChildState;
			int rState = parent.rChildState;
			int split = parent.split;

			HyperEdge newParentL = null, newParentR = null;
			if (lState != -1 && grammarTags[lState] ) { // left is not a POS
				int lBest = parent.lChildBest + 1;
				HyperEdge lChild = chartAfterU[start][split][lState]
						.getKbest(lBest);
				if (lChild != null) {
					int rBest = parent.rChildBest;
					HyperEdge rChild = chartAfterU[split][end][rState]
							.getKbest(rBest);
					double newScore = lChild.score + rChild.score + ruleScore;
					newParentL = new HyperEdge(pState, lState, rState,
							suboptimalities, lBest, rBest, start, split, end,
							newScore, ruleScore);
					// chartBeforeU[start][end][pState].addToFringe(newParentL);
				}
			}
			if (rState != -1 && grammarTags[rState]) {
				int rBest = parent.rChildBest + 1;
				HyperEdge rChild = chartAfterU[split][end][rState]
						.getKbest(rBest);
				if (rChild != null) {
					int lBest = parent.lChildBest;
					HyperEdge lChild = chartAfterU[start][split][lState]
							.getKbest(lBest);
					double newScore = lChild.score + rChild.score + ruleScore;
					newParentR = new HyperEdge(pState, lState, rState,
							suboptimalities, lBest, rBest, start, split, end,
							newScore, ruleScore);
					// chartBeforeU[start][end][pState].addToFringe(newParentR);
				}
			}

			if (newParentL != null && newParentR != null
					&& newParentL.score > newParentR.score)
				chartBeforeU[start][end][pState].addToFringe(newParentL);
			else if (newParentL != null && newParentR != null)
				chartBeforeU[start][end][pState].addToFringe(newParentR);
			else if (newParentL != null || newParentR != null) {
				if (newParentL != null)
					chartBeforeU[start][end][pState].addToFringe(newParentL);
				else
					/* newParentR!=null */chartBeforeU[start][end][pState]
							.addToFringe(newParentR);
			}
			parent.alreadyExpanded = true;

			// chartBeforeU[start][end][pState].expandNextBest();
		} else { // unary
		// if (chartAfterU[start][end][pState].sortedListSize() >=
		// suboptimalities) return; // already have enough derivations

			int cState = parent.childState;
			int cBest = parent.childBest + 1;

			if (grammarTags[cState]) {
				HyperEdge child = chartBeforeU[start][end][cState]
						.getKbest(cBest);
				if (child != null) {
					double newScore = child.score + ruleScore;
					HyperEdge newParent = new HyperEdge(pState, cState,
							suboptimalities, cBest, start, end, newScore,
							ruleScore);
					// if (newScore>=parent.score)
					// System.out.println("ullala");
					chartAfterU[start][end][pState].addToFringe(newParent);
				}
				parent.alreadyExpanded = true;
				// chartAfterU[start][end][pState].expandNextBest();
			}
		}
	}


	public CoarseToFineNBestParser newInstance() {
		CoarseToFineNBestParser newParser = new CoarseToFineNBestParser(
				grammar, lexicon, kBest, endLevel, viterbiParse,
				outputSub, outputScore, accurate,
				useGoldPOS, false, latticeWeight, outputChartSize);
		newParser.initCascade(this);
		return newParser;
	}	

	public void initCascade(CoarseToFineNBestParser otherParser) {
		lChildMap = otherParser.lChildMap;
		rChildMap = otherParser.rChildMap;
		grammarCascade = otherParser.grammarCascade;
		lexiconCascade = otherParser.lexiconCascade;
	}


	public synchronized Object call() {
		List<Tree<String>> result = getKBestConstrainedParses(nextLattice,
				null, kBest);
		nextLattice = null;
		synchronized (queue) {
			queue.add(result, -nextSentenceID);
			queue.notifyAll();
		}
		return null;
	}
	
	public double getLogLikelihood(Tree<String> tree) {
		if (logLikelihood == Double.NEGATIVE_INFINITY)
			return logLikelihood;

		if (viterbiParse)
			return logLikelihood;
		ArrayList<Tree<String>> resultList = new ArrayList<Tree<String>>();
		Tree<String> newTree = TreeAnnotations.processTree(tree, 1, 0,
				binarization, false);
		resultList.add(newTree);
		StateSetTreeList resultStateSetTrees = new StateSetTreeList(resultList,
				grammar.numSubStates, false, tagNumberer);
		ArrayParser llParser = new ArrayParser(grammar, lexicon);
		for (Tree<StateSet> t : resultStateSetTrees) {
			llParser.doInsideScores(t, false, false, null); // Only inside
															// scores are needed
															// here
			double ll = Math.log(t.getLabel().getIScore(0));
			ll += 100 * t.getLabel().getIScale();
			return ll;
		}
		return Double.NEGATIVE_INFINITY;

	}

	public double getLogLikelihood() {
		if (logLikelihood == Double.NEGATIVE_INFINITY)
			return logLikelihood;

		if (viterbiParse)
			return logLikelihood;

		logLikelihood = Math.log(iScore[0][length][0][0]);// +

		return logLikelihood;
	}

	public double getConfidence(Tree<String> tree) {
		if (logLikelihood == Double.NEGATIVE_INFINITY)
			return logLikelihood;
		// try{
		double treeLL = getLogLikelihood(tree);
		double sentenceLL = getLogLikelihood();
		return treeLL - sentenceLL;
		// } catch (Exception e){
		// System.err.println("Couldn't compute LL of tree: " + tree);
		// return Double.NEGATIVE_INFINITY;
		// }

	}
	public double getModelScore(Tree<String> parsedTree) {
		//if (viterbiParse)
			return logLikelihood;
		// 	return savedScore;

	}


    public int chartSize(Grammar curGram, boolean log)
	{
    	int constituents = 0;
    	double defVal = Double.NEGATIVE_INFINITY;
    	if (!log)
    		defVal = 0.0;
        int startDiff = (level < 0) ? 2 : 1;
        for (int diff = startDiff; diff <= length; diff++) 
        {
            for (int start = 0; start < (length - diff + 1); start++) 
            {
                int end = start + diff;
                for (int state = 0; state != numStates; state++)
                {
                    for (int subState = 0; subState != curGram.numSubStates[state]; subState++)
                        if (iScore[start][end][state] != null && iScore[start][end][state][subState] != defVal)
                        {
                        		constituents +=1;
                        }
                }
            }
        }
        return constituents;
    }

    public void printChart(Grammar curGram, Boolean log)
	{
    	double defVal = Double.NEGATIVE_INFINITY;
    	if (!log)
    		defVal = 0.0;
        int startDiff = (level < 0) ? 2 : 1;
        for (int diff = startDiff; diff <= length; diff++) {
            for (int start = 0; start < (length - diff + 1); start++) {
                System.out.println();
                int end = start + diff;
                for (int state = 0; state != numStates; state++)
                    for (int subState = 0; subState != curGram.numSubStates[state]; subState++)
                        if (iScore[start][end][state] != null && iScore[start][end][state][subState] != defVal)
                        {
                        	String word = (words[start][end][state][subState] == -1) ? "" : wordIds.getWord(words[start][end][state][subState]);// = wordIds.getId(trans.word);

                            String stateStr = (String) tagNumberer.object(state);
                            if (log)
                            	System.out.println(start + "," + end + "  " + stateStr  + "_" + subState + 
                            						'\t'+Math.exp(iScore[start][end][state][subState]) + '\t' + word);
                            else
                            	System.out.println(start + "," + end + "  " + stateStr  + "_" + subState + 
                						'\t'+ iScore[start][end][state][subState] + '\t' + word);
                        }
            }
        }
    }
   
    public void printChartLevel0(Grammar curGram)
    {
        int startDiff = (level < 0) ? 2 : 1;
        for (int diff = startDiff; diff <= length; diff++) {
            for (int start = 0; start < (length - diff + 1); start++) {
                System.out.println();
                int end = start + diff;
                for (int state = 0; state != numStates; state++)
                    if (viScore[start][end] != null && viScore[start][end][state] != Double.NEGATIVE_INFINITY)
                    {
                        String stateStr = (String) tagNumberer.object(state);
                        System.out.println(start + "," + end + "  " + stateStr  + " \t" + Math.exp(viScore[start][end][state]));
                }
            }
        }
    }
	public void printMaxCScores(Grammar curGram)
	{
		int startDiff = (level < 0) ? 2 : 1;
		for (int diff = startDiff; diff <= length; diff++) {
			for (int start = 0; start < (length - diff + 1); start++) {
				System.out.println();
				int end = start + diff;
				for (int state = 0; state != numStates; state++)
				{
					if (chartAfterU[start][end][state] != null && chartAfterU[start][end][state].nSorted > 0 && 
							chartAfterU[start][end][state].getKbest(1)!= null)
					{
						String stateStr = (String) tagNumberer.object(state);
						String childState = (String) tagNumberer.object(chartAfterU[start][end][state].getKbest(1).childState); 
						String childState2 = (String) tagNumberer.object(chartBeforeU[start][end][state].getKbest(1).childState); 

						System.out.println(start + "," + end + "  " + stateStr  + " \t" +childState + "\t" + childState2); 
					}
				}
			}
		}
	}

	public String idToString(int id)
	{
		return (String) tagNumberer.object(id);
	}
}
