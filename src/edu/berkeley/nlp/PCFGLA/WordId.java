package edu.berkeley.nlp.PCFGLA;

import java.util.HashMap;
import java.util.Vector;

public class WordId {

	private Vector<String> wordsVec;
	private HashMap<String, Integer> wordsMap;

	public WordId() 
	{
		wordsMap = new HashMap<String, Integer>();
		wordsVec = new Vector<String>();
	}
	
	public int getId(String word)
	{
		if (wordsMap.containsKey(word))
			return wordsMap.get(word);
		else
		{
			int wordId = wordsVec.size();
			wordsVec.add(word);
			wordsMap.put(word, wordId);
			return wordId;
		}
	}
	
	public String getWord(int id)
	{
		return wordsVec.get(id);
	}
}
