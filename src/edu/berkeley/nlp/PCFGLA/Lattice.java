package edu.berkeley.nlp.PCFGLA;

import java.util.Vector;

public class Lattice {
	public Vector<Vector<Transition>> begHash = new Vector<Vector<Transition>>();

	public Lattice() 
	{
	}
	
	public void add(String line)
	{
		Transition newTrans = new Transition(line);
		while (newTrans.beg >= begHash.size())
		{
			begHash.add(new Vector<Transition>());//ensureCapacity(newTrans.beg+1);
		}
		begHash.get(newTrans.beg).add(newTrans);
	}
	
	public int size()
	{
		return begHash.size();
	}
	
	public Vector<Transition> getTransPos(int loc)
	{
		if (loc >= begHash.size())
			return new Vector<Transition>();
		return begHash.get(loc);
	}
	
	public Vector<Transition> getTransitions(int beg, int end)
	{
		Vector<Transition> result = new Vector<Transition>();
		for (Transition itr: begHash.get(beg))
			if (itr.end == end)
				result.add(itr);
		return result;
	}
	public double getProb(int beg, int end)
	{
		for (Transition itr: begHash.get(beg))
			if (itr.end == end)
				return itr.prob;
		return 0.0;
	}
}
