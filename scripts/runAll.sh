# If more than 5 arguments are given, it will run using slurm
function run {
    if [ "$6" ];
    then
        python3 scripts/pg.prep.py $1 $2 $3 $4 $5
        python3 scripts/pg.run.py $2.[0-9]*
    else
        chmod +x $1
        bash $1
    fi
}

./scripts/0.norm.prep.sh
./scripts/0.norm.train.sh > 0.train.sh
run 0.train.sh 0.train 2 40 8 $1

./scripts/1.numCands.run.sh > 1.numCands.sh
run 1.numCands.sh 1.numCands 1 32 1 $1
python3 scripts/runPG.py runGraph.*

./scripts/2.weight.run.sh > 2.weight.sh
run 2.weight.sh 2.weight 1 32 1 $1

./scripts/3.test.prep.sh
python3 ./scripts/3.test.run.py > 3.test.sh
run 3.test.sh 3.test 1 32 1 $1

