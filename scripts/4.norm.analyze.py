
pred = []
predFile = open('preds/4.lexnorm.txt', errors='ignore', encoding='utf-8')
for line in predFile:
    pred.append(line.split())

raw = []
gold = []
rawLine = []
goldLine = []
for line in open('../monoise/data/en2/test', errors='ignore', encoding='utf-8'):
    tok = line.split()
    if line.strip().isdigit():
        if rawLine == []:
            continue
        raw.append(rawLine)
        gold.append(goldLine)
        rawLine = []
        goldLine = []
    else:
        if len(tok) != 3:
            rawLine.append('')
            goldLine.append('')
        else:
            rawLine.append(tok[0])
            goldLine.append(tok[2])

total = 0
needNorm = 0
cor = 0
inCor = 0
print(len(pred), len(gold), len(raw), '\n')
for i in range(len(gold)):
    if len(pred[i]) != len(gold[i]):
        continue
    for j in range(len(pred[i])):
        gold[i][j] = gold[i][j].lower()
        raw[i][j] = raw[i][j].lower()
        pred[i][j] = pred[i][j].lower()
        if gold[i][j] == raw[i][j]:
            if pred[i][j] != gold[i][j]:
                inCor += 1
                print(pred[i][j], gold[i][j])
        else:
            if pred[i][j] == gold[i][j]:
                cor += 1
            needNorm += 1
        total += 1
     
print(cor, inCor, total, needNorm)
print("Acc: ", cor / total)
print("baseline: ", 1-(needNorm/total))
print("baseline: ", 1-(needNorm/total))


