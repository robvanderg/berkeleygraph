mkdir -p preds

for CANDS in {1..9};
do
    for SEED in {0..9}; 
    do 
        echo "cd ../monoise/src && ./tmp/bin/binary -m RU -d ../data/en/ -i ../../berkeleygraph/data/twitterdev_tokens -r ../working/chenliCaps.unk.$SEED -c $CANDS -C -W -u ../../berkeleygraph/knowns | java -jar ../utils/BerkeleyGraph.jar -latticeWeight 2 -gr ../../berkeleygraph/ewtwsj.gr > ../../berkeleygraph/preds/1.twitterdev.U.$CANDS.$SEED.ptb"
        echo "cd ../monoise/src && ./tmp/bin/binary -m RU -d ../data/en/ -i ../../berkeleygraph/data/twitterdev_tokens -r ../working/chenliCaps.$SEED -c $CANDS -C -W | java -jar ../utils/BerkeleyGraph.jar -latticeWeight 2 -gr ../../berkeleygraph/ewtwsj.gr > ../../berkeleygraph/preds/1.twitterdev.A.$CANDS.$SEED.ptb"
    done
done

