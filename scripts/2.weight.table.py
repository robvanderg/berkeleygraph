
def getPerf(path):
    return float(open(path).readline().split()[-1])

print("\\begin{table}")
print("    \\centering")
print("    \\begin{tabular}{l | c c}")
print("        \\toprule")
print("        Weight($\\beta$) & \\texttt{ALL} ($\\alpha=1$) & \\texttt{UNK} ($\\alpha=6$) \\\\")
print("        \\midrule")
unks = []
alls = []
for weight in ["0.125", "0.25", "0.5", "1", "2", "4", "8", "16"]:
    unk = 0.0
    all = 0.0
    for seed in range(0,9):
        unk += getPerf('.'.join(['preds/2.twitterdev.U', '6', str(seed), weight, 'ptb.eval']))
        all += getPerf('.'.join(['preds/2.twitterdev.A', '1', str(seed), weight, 'ptb.eval']))
    unks.append(unk/len(range(0,9)))
    alls.append(all/len(range(0,9)))
for weightIdx, weight in enumerate(["0.125", "0.25", "0.5", "1", "2", "4", "8", "16"]):
    print("        " + weight.ljust(15), end='& ' )
    printAll = str(round(alls[weightIdx], 2)).ljust(5,'0')
    if alls[weightIdx] == max(alls):
        printAll = '\\textbf{' + printAll + '}'
    print(printAll + ' &    ', end='')

    printUnk = str(round(unks[weightIdx], 2)).ljust(5,'0')
    if unks[weightIdx] == max(unks):
        printUnk = '\\textbf{' + printUnk + '}'
    print(printUnk + ' \\\\')
print("        \\bottomrule")
print("    \\end{tabular}")
print("    \\caption{F1 scores on the development data using different weights, comparing only using the best candidate versus using 6 candidates.}")
print("    \\label{tab:weights}")
print("\\end{table}")

