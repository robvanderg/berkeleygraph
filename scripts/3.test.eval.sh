
for FILE in preds/3*test*.ptb
do
    ./EVALB/evalb -p EVALB/rob.prm data/twittertest_gold $FILE | grep "Bracketing FM" | head -1 > $FILE.eval
done

for FILE in preds/3*dev*.ptb
do
    ./EVALB/evalb -p EVALB/rob.prm data/twitterdev_gold $FILE | grep "Bracketing FM" | head -1 > $FILE.eval
done

