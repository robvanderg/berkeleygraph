import sys

if len(sys.argv) < 3:
    print('usage: convert.py input output')

outFile = open(sys.argv[2], 'w')

wordCounter = 0
for line in open(sys.argv[1], 'r', errors='ignore', encoding='utf-8'):
    if line.strip() == '':
        outFile.write('.\n')
        wordCounter = 0
        continue
    outFile.write(str(wordCounter) + ' ' + line.strip() + ' ' + str(wordCounter+1) + ' 1.0\n');
    wordCounter += 1
outFile.close()
