
def getPerf(path):
    return float(open(path).readline().split()[-1])

print("\\begin{table}")
print("    \\centering")
print("    \\begin{tabular}{l l l}")
print("        \\toprule")
print("        Parser              & \\hspace{0.19cm} dev & \\hspace{0.19cm} test \\\\")

print("        \\midrule")
print("        Stanford Parser     & 66.05 & 61.95 \\\\")
print("        Berkeley Parser     & ", end='') 
print(str(round(getPerf('preds/3.Berkeley.dev.ptb.eval'), 2)), end = ' & ')
print(str(round(getPerf('preds/3.Berkeley.test.ptb.eval'), 2)), end = ' \\\\\n')

print("        \midrule")
print("        Best norm. seq.     & ", end='')
print(str(round(getPerf('preds/3.bestSeq.dev.ptb.eval'), 2)), end = ' & ')
print(str(round(getPerf('preds/3.bestSeq.test.ptb.eval'), 2)), end = ' \\\\\n')
print("        Integrated norm.    & ", end='')
print(str(round(getPerf('preds/3.integr.dev.ptb.eval'), 2)), end = '\\textsuperscript{*} & ')
print(str(round(getPerf('preds/3.integr.test.ptb.eval'), 2)), end = '\\textsuperscript{*} \\\\\n')

print("        \midrule")
print("        Gold POS tags       & 74.98 & 71.80 \\\\")
print("        \\bottomrule")
print("        \\end{tabular}")
print("    \\caption{F1 scores of our proposed models and previous work on the test set, trained on the EWT and WSJ. \\textsuperscript{*}Statistical significant against Berkeley Parser at $p < 0.01$ and at $p < 0.05$ against the best normalization sequence using a paired t-test.}")
print("    \\label{tab:parse}")
print("\\end{table}")

