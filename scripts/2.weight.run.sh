mkdir -p preds


for WEIGHT in "0.125" "0.25" "0.5" 1 2 4 8 16;
do
    for CANDS in 1 6;
    do
        for SEED in {0..9}; 
        do 
            echo "cd ../monoise/src && ./tmp/bin/binary -m RU -d ../data/en/ -i ../../berkeleygraph/data/twitterdev_tokens -r ../working/chenliCaps.unk.$SEED -c $CANDS -C -W -u ../../berkeleygraph/knowns | java -jar ../utils/BerkeleyGraph.jar -latticeWeight $WEIGHT -gr ../../berkeleygraph/ewtwsj.gr > ../../berkeleygraph/preds/2.twitterdev.U.$CANDS.$SEED.$WEIGHT.ptb"
            echo "cd ../monoise/src && ./tmp/bin/binary -m RU -d ../data/en/ -i ../../berkeleygraph/data/twitterdev_tokens -r ../working/chenliCaps.$SEED -c $CANDS -C -W | java -jar ../utils/BerkeleyGraph.jar -latticeWeight $WEIGHT -gr ../../berkeleygraph/ewtwsj.gr > ../../berkeleygraph/preds/2.twitterdev.A.$CANDS.$SEED.$WEIGHT.ptb"
        done
    done
done

