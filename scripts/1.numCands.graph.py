import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import sys

import myutils

fig, ax = plt.subplots(figsize=(8,5), dpi=300)

def getPerf(path):
    return float(open(path).readline().split()[-1])

unk = []
all = []
for cands in range(1,10):
    unk.append(0.0)
    all.append(0.0)
    for seed in range(0,9):
        unk[-1] += getPerf('preds/1.twitterdev.U.' + str(cands) + '.' + str(seed) + '.ptb.eval')
        all[-1] += getPerf('preds/1.twitterdev.A.' + str(cands) + '.' + str(seed) + '.ptb.eval')
    unk[-1] /= len(range(0,9))
    all[-1] /= len(range(0,9))

print(unk)
print(all)

ax.plot(range(1,len(all)+1), all, 'ro--', color = myutils.colors[0], markersize=8, label='ALL')
ax.plot(range(1,len(unk)+1), unk, 'b^--', color = myutils.colors[1], markersize=8, label='UNK')
ax.plot([1,len(unk)+1], [70.85,70.85], linestyle='dashed', color=myutils.colors[2], label='Berkeley')
ax.legend()
ax.set_xlim(.9,9.1)
ax.set_ylabel('F1-score')
ax.set_xlabel(r'Number of normalization candidates used ($\alpha$)')
fig.tight_layout()
fig.savefig('numCands.pdf', bbox_inches='tight')

