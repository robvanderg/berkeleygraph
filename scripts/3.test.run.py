

for dataset in ['dev', 'test']:
    cmd = 'cat data/twitter' + dataset + '_fsa | java -jar BerkeleyGraph.jar -gr ewtwsj.gr > preds/3.Berkeley.' + dataset + '.ptb'
    print(cmd)
    cmd = 'cd ../monoise/src && ./tmp/bin/binary -m RU -d ../data/en/ -i ../../berkeleygraph/data/twitter' + dataset + '_tokens -r ../working/chenliCaps.unk.4 -c 1 -C -W -u ../../berkeleygraph/knowns | java -jar ../utils/BerkeleyGraph.jar -latticeWeight 2 -gr ../../berkeleygraph/ewtwsj.gr > ../../berkeleygraph/preds/3.bestSeq.' + dataset + '.ptb'
    print(cmd)
    cmd = 'cd ../monoise/src && ./tmp/bin/binary -m RU -d ../data/en/ -i ../../berkeleygraph/data/twitter' + dataset + '_tokens -r ../working/chenliCaps.unk.4 -c 6 -C -W -u ../../berkeleygraph/knowns | java -jar ../utils/BerkeleyGraph.jar -latticeWeight 2 -gr ../../berkeleygraph/ewtwsj.gr > ../../berkeleygraph/preds/3.integr.' + dataset + '.ptb'
    print(cmd)
